# pdfcontroller

PDF파일의 위변조를 막기 위해 해시값을 블록체인에 트랜잭션 형태로 기록한다.

해시값을 기록한 트랜잭션을 스탬프 트랜잭션이라 부르며 이 트랜잭션의 아이디와 아웃풋 인덱스를 조합해서 스탬프 아이디를 만들고
원본 PDF파일을 훼손하지 않도록 Signature 영역에 incremental save 방식으로 스탬프 아이디를 저장한다.


## Stamp ID

트랜잭션 아이디와 아웃풋 인덱스를 조합해서 스탬프 아이디를 만들고
원본 해시값이 필요할 때 블록체인을 조회하는 용도로 사용한다.

Stamp ID = Transaction ID + "-" + Output Index

eg. `2b42ae6c1f677f24eb1ab1a0eba2ae8fe6a2de93b7088f5d3ce3e4e5a3372bbf-0`


## Signature

Signature를 생성하여 스탬프 아이디와 원본 문서 크기를 함께 기록하고,
incremental save 방식으로 저장하여 원본 PDF 문서를 훼손하지 않는 형태로 삽입한다.

별도 인증서를 지정하지 않으면 라이브러리에 기본으로 포함된 Self-signed 인증서를 사용하여 Signature를 생성한다.
