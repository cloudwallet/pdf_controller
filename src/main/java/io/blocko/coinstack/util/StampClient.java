package io.blocko.coinstack.util;

import java.io.IOException;
import java.util.Date;

import io.blocko.apache.commons.codec.DecoderException;
import io.blocko.apache.commons.codec.binary.Hex;
import io.blocko.bitcoinj.core.Address;
import io.blocko.bitcoinj.core.Utils;
import io.blocko.bitcoinj.params.MainNetParams;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.TransactionBuilder;
import io.blocko.coinstack.TransactionUtil;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack.model.Transaction;

public class StampClient {
	
	public static class StampExt extends Stamp {
		private String hash;
		
		public StampExt(String txId, int outputIndex, int confirmations, Date timestamp, String hash) {
			super(txId, outputIndex, confirmations, timestamp);
			this.hash = hash;
		}
		
		public String getHash() {
			return hash;
		}
	}
	
	public static String stampDocument(CoinStackClient coinstack, char[] privateKeyWIF, String documentHash) throws IOException, CoinStackException {
		return stampDocument(coinstack, new String(privateKeyWIF), documentHash);
	}
	
	public static String stampDocument(CoinStackClient coinstack, String privateKeyWIF, String documentHash) throws IOException, CoinStackException {
		//return coinstack.stampDocument(privateKeyWIF, documentHash);
		if (documentHash == null || documentHash.length() != 64) {
			throw new MalformedInputException("Invalid input",
					"Invalid hash format - needs to be hex encoded");
		}
		StringBuilder hexBuffer = new StringBuilder();
		hexBuffer.append("5331");
		hexBuffer.append(documentHash);
		byte[] hexData = null;
		try {
			hexData = Hex.decodeHex(hexBuffer.toString().toCharArray());
		}
		catch (DecoderException e) {
			throw new MalformedInputException("Invalid input",
					"Invalid hash format - needs to be hex encoded");
		}
		// build transaction
		TransactionBuilder dataTx = new TransactionBuilder();
		dataTx.shuffleOutputs(false);
		dataTx.allowDustyOutput(true);
		dataTx.setData(hexData);
		dataTx.setFee(io.blocko.coinstack.Math.convertToSatoshi("0.0001"));
		
		// find output index
		int outIdx = 0;
		Output[] outs = dataTx.getOutputs();
		for (int x=0, len=outs.length; x<len; x++) {
			Output out = outs[x];
			if (out != null && out.getValue() == 0) {
				outIdx = x;
				break;
			}
		}
		
		// send transaction
		String signedDataTx = coinstack.createSignedTransaction(dataTx, privateKeyWIF);
		String txHash = TransactionUtil.getTransactionHash(signedDataTx, coinstack.isMainNet());
		coinstack.sendTransaction(signedDataTx);
		
		String stampId = txHash + "-" + outIdx;
		return stampId;
	}
	
	private static StampExt getStampExt(CoinStackClient coinstack, String stampId)
			throws IOException, CoinStackException {
		//return coinstack.getStampIndirect(stampId);
		String[] splitted = stampId.split("-");
		if (splitted == null || splitted.length < 2) {
			throw new MalformedInputException("Invalid input",
					"Invalid stampId format - need to have a dash");
		}
		String txId = splitted[0];
		int vout = Integer.valueOf(splitted[1]);
		Transaction tx = coinstack.getTransaction(txId);
		Output out = tx.getOutputs()[vout];
		String data = Hex.encodeHexString(out.getData());
		String hash = data.substring(4);
		Date timestamp = null;
		int confirmations = 0;
		int[] blockHeights = tx.getBlockHeights();
		if (blockHeights != null && blockHeights.length > 0) {
			BlockchainStatus status = coinstack.getBlockchainStatus();
			confirmations = status.getBestHeight() - blockHeights[0] + 1;
			timestamp = tx.getConfirmationTime();
		}
		StampExt stamp = new StampExt(txId, vout, confirmations, timestamp, hash);
		return stamp;
	}
	
	private static StampExt getStampRedirect(CoinStackClient coinstack, String stampId)
			throws IOException, CoinStackException {
		try {
			StampExt stamp = getStampExt(coinstack, stampId);
			if (stamp != null) {
				return stamp;
			}
		} catch (Exception e) {
			// do nothing
		}
		// search redirected stamp
		String redirectIndex = getRedirectIndex(stampId);
		String redirectedStampId = getRedirectedStampId(coinstack, redirectIndex);
		StampExt stamp = getStampExt(coinstack, redirectedStampId);
		return stamp;
	}
	protected static String getRedirectIndex(String stampId) {
		byte[] hash160 = Utils.sha256hash160(stampId.getBytes());
		return new Address(MainNetParams.get(), hash160).toString();
	}
	protected static String getRedirectedStampId(CoinStackClient coinstack, String redirectIndex)
			throws IOException, CoinStackException {
		String[] history = coinstack.getTransactions(redirectIndex);
		if (history == null || history.length == 0) {
			return null;
		}
		for (String txId : history) {
			Transaction tx = coinstack.getTransaction(txId);
			if (tx == null) {
				continue;
			}
			Output[] outputs = tx.getOutputs();
			int cnt = (outputs == null) ? 0 : outputs.length;
			for (int x=0; x<cnt; x++) {
				Output out = outputs[x];
				if (out == null) {
					continue;
				}
				byte[] data = out.getData();
				if (data == null || data.length == 0) {
					continue;
				}
				String hex = Hex.encodeHexString(data);
				if (hex.startsWith("5331")) {
					return txId + "-" + x;
				}
			}
		}
		return null;
	}
	
	
	public static String getStampData(CoinStackClient coinstack, String stampId) throws IOException, CoinStackException {
		return getStampRedirect(coinstack, stampId).getHash();
	}
	public static int getStampConfirmations(CoinStackClient coinstack, String stampId) throws IOException, CoinStackException {
		return getStampRedirect(coinstack, stampId).getConfirmations();
	}
	public static Stamp getStamp(CoinStackClient coinstack, String stampId) throws IOException, CoinStackException {
		return getStampRedirect(coinstack, stampId);
	}
	
}
