package io.blocko.coinstack.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;

public class PDFController extends StampSignerBC implements SignatureInterface {
	
	protected static class StampInfo {
		
		public static final String SIGNATURE_NAME = "coinstack";
		public static final String SIGNATURE_DELIM = "/";
		
		String stampId;
		int size;
		
		public StampInfo(String stampId, int size) {
			this.stampId = stampId;
			this.size = size;
		}
		
		public static StampInfo loadLegacyWithName(String source) {
			return loadLegacy(source, SIGNATURE_DELIM, 1, 2);
		}
		public static StampInfo loadLegacyWithReason(String source) {
			return loadLegacy(source, SIGNATURE_DELIM, 0, 1);
		}
		public static StampInfo loadLegacy(String source, String delim, int stampIdIndex, int sizeIndex) {
			String[] tokens = source.split(delim);
			if (tokens == null || tokens.length < sizeIndex+1) {
				throw new RuntimeException("MalformedStampInfo Exception");
			}
			String stampId = tokens[stampIdIndex];
			int size = Integer.valueOf(tokens[sizeIndex]);
			return new StampInfo(stampId, size);
		}
		public static StampInfo load(String stampId, String size) {
			int originalSize = Integer.valueOf(size);
			return new StampInfo(stampId, originalSize);
		}
		
		public String getStampId() {
			return stampId;
		}
		public int getSize() {
			return size;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(stampId);
			sb.append(SIGNATURE_DELIM);
			sb.append(size);
			return sb.toString();
		}
		
		public static boolean accept(String source) {
			return (source != null && source.startsWith(SIGNATURE_NAME));
		}
	}
	
	
	private byte[] source = null;
	private int size = -1;
	private PDDocument doc = null;
	private StampInfo stamp = null;
	private SignatureInterface signer = null;
	private SignatureOptions options = null;
	
	public PDFController(byte[] input) {
		init(input);
	}
	public PDFController(InputStream input) {
		try {
			byte[] buffer = loadStream(input);
			init(buffer);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	public PDFController(File input) {
		try {
			InputStream is = new FileInputStream(input);
			byte[] buffer = loadStream(is);
			init(buffer);
			is.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	private static byte[] loadStream(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(in, out);
		return out.toByteArray();
	}
	
	
	private void init(byte[] input) {
		this.source = input;
		this.size = source.length;
		try {
			this.doc = PDDocument.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.stamp = loadStampInfo();
	}
	public void close() {
		this.source = null;
		this.size = -1;
		try {
			if (doc != null) {
				doc.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.doc = null;
		this.stamp = null;
	}
	
	public void setSigner(SignatureInterface signer) {
		this.signer = signer;
	}
	public SignatureInterface getSigner() {
		return (this.signer != null) ? this.signer : this;
	}
	public void setOptions(SignatureOptions options) {
		this.options = options;
	}
	public SignatureOptions getOptions() {
		return (this.options != null) ? this.options : new SignatureOptions();
	}
	
	
	private StampInfo loadStampInfo() {
		try {
			List<PDSignature> list = doc.getSignatureDictionaries();
			for (PDSignature item : list) {
				String name = (item == null) ? null : item.getName();
				if (name == null || !name.startsWith(StampInfo.SIGNATURE_NAME)) {
					continue;
				}
				if (name != null && name.indexOf(StampInfo.SIGNATURE_DELIM) > 0) {
					return StampInfo.loadLegacyWithName(name);
				}
				String reason = item.getReason();
				if (reason != null && reason.indexOf(StampInfo.SIGNATURE_DELIM) > 0) {
					return StampInfo.loadLegacyWithReason(reason);
				}
				String location = item.getLocation();
				return StampInfo.load(reason, location);
			}
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public boolean isEncrypted() {
		return doc.isEncrypted();
	}
	public boolean hasStampId() {
		return (stamp != null);
	}
	public String getStampId() {
		if (stamp == null) {
			throw new RuntimeException("StampNotFoundException");
		}
		return stamp.getStampId();
	}
	public int getStampSize() {
		if (stamp == null) {
			throw new RuntimeException("StampNotFoundException");
		}
		return stamp.getSize();
	}
	
	public String calculateHashStamped() {
		if (stamp == null) {
			throw new RuntimeException("StampNotFoundException");
		}
		//byte[] hash = CodecUtils.digest(source, stamp.getSize());
		byte[] hash = io.blocko.bitcoinj.core.Utils.singleDigest(source, 0, stamp.getSize());
		return io.blocko.bitcoinj.core.Utils.HEX.encode(hash);
	}
	public String calculateHash() {
		//byte[] hash = CodecUtils.digest(source);
		byte[] hash = io.blocko.bitcoinj.core.Utils.singleDigest(source, 0, source.length);
		return io.blocko.bitcoinj.core.Utils.HEX.encode(hash);
	}
	
	public void writeStampId(File out, String stampId) throws IOException {
		OutputStream os = new FileOutputStream(out);
		writeStampId(os, stampId);
		os.close();
	}
	public void writeStampId(OutputStream out, String stampId) throws IOException {
		PDSignature signature = new PDSignature();
        signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
        //signature.setSubFilter(PDSignature.SUBFILTER_ADBE_X509_RSA_SHA1);
        signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
		signature.setName(StampInfo.SIGNATURE_NAME);
		signature.setReason(stampId);
		signature.setLocation(String.valueOf(size));
		signature.setSignDate(Calendar.getInstance());
		doc.addSignature(signature, this.getSigner(), this.getOptions());
		doc.saveIncremental(out);
	}
	@Deprecated
	public void writeStampId(OutputStream out, String stampId, SignatureOptions options) throws IOException {
		this.setOptions(options);
		this.writeStampId(out, stampId);
	}
	
	
	public void changeStampId(File out, String stampId) throws IOException {
		OutputStream os = new FileOutputStream(out);
		changeStampId(os, stampId);
		os.close();
	}
	public void changeStampId(OutputStream out, String stampId) throws IOException {
		PDFController controller = this.getOriginController();
		controller.setSigner(this.getSigner());
		controller.setOptions(this.getOptions());
		controller.writeStampId(out, stampId);
		controller.close();
	}
	@Deprecated
	public void changeStampId(OutputStream out, String stampId, SignatureOptions options) throws IOException {
		this.setOptions(options);
		this.changeStampId(out, stampId);
	}
	
	public PDFController getOriginController() {
		int originSize = getStampSize();
		byte[] originSource = new byte[originSize];
		System.arraycopy(source, 0, originSource, 0, originSize);
		PDFController controller = new PDFController(originSource);
		return controller;
	}
}
