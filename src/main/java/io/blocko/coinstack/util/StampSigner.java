package io.blocko.coinstack.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;

import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import io.blocko.apache.commons.io.IOUtils;

@SuppressWarnings("restriction")
public class StampSigner implements SignatureInterface {
	
	public static final String CONFIG_XML = "coinstack/coinstack-stamp.xml";
	public static final String STAMP_KEYSTORE_PATH = "stamp.keystore.path";
	public static final String STAMP_KEYSTORE_PIN = "stamp.keystore.pin";
	
	
	private PrivateKey privateKey;
	private Certificate[] certificateChain;
	
	private final void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	private final PrivateKey getPrivateKey() {
		return this.privateKey;
	}
	private final void setCertificateChain(final Certificate[] certificateChain) {
		this.certificateChain = certificateChain;
	}
	private final Certificate[] getCertificateChain() {
		return this.certificateChain;
	}
	private final Certificate getCertificate(int index) {
		return getCertificateChain()[index];
	}
	
	
	private boolean isLoaded = false;
	
	public void loadSigner() {
		loadSigner(CONFIG_XML);
	}
	public void loadSigner(String configPath) {
		try {
			Properties props = loadProperties(configPath);
			loadSigner(props);
		} catch (GeneralSecurityException e) {
			throw new IllegalArgumentException(e);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
	public void loadSigner(Properties props)
			throws GeneralSecurityException, IOException {
		String path = props.getProperty(STAMP_KEYSTORE_PATH);
		String pin = props.getProperty(STAMP_KEYSTORE_PIN);
		loadSigner(path, pin.toCharArray());
	}
	public void loadSigner(String resourcePath, char[] pin)
			throws GeneralSecurityException, IOException {
		ClassLoader cl = this.getClass().getClassLoader();
		InputStream is = cl.getResourceAsStream(resourcePath);
		loadSigner(is, pin);
		is.close();
	}
	public void loadSigner(InputStream is, char[] pin)
			throws GeneralSecurityException, IOException {
		// set flag
		isLoaded = true;
		
		// load keystore
		KeyStore keystore = KeyStore.getInstance("PKCS12");
		keystore.load(is, pin);
		
		// load private key and certificate chain
		Certificate cert = null;
		String alias;
		Enumeration<String> aliases = keystore.aliases();
		while (aliases.hasMoreElements()) {
			alias = aliases.nextElement();
			PrivateKey privkey = (PrivateKey) keystore.getKey(alias, pin);
			setPrivateKey(privkey);
			Certificate[] certChain = keystore.getCertificateChain(alias);
			if (certChain == null) {
				continue;
			}
			setCertificateChain(certChain);
			cert = certChain[0];
			if (cert instanceof X509Certificate) {
				// avoid expired certificate
				((X509Certificate) cert).checkValidity();
			}
			break;
		}
		if (cert == null) {
			throw new IOException("Could not find certificate");
		}
	}
	
	
	@Override
	public byte[] sign(InputStream content) throws IOException {
		try {
			if (!isLoaded) {
				loadSigner();
			}
			byte[] dataToSign = IOUtils.toByteArray(content);
			X509Certificate cert = (X509Certificate) getCertificate(0);
			Signature signature = Signature.getInstance("Sha1WithRSA");
			signature.initSign(getPrivateKey());
			signature.update(dataToSign);
			byte[] signedData = signature.sign();
			
			//load X500Name
			X500Name xName = X500Name.asX500Name(cert.getSubjectX500Principal());
			//load serial number
			BigInteger serial = cert.getSerialNumber();
			//laod digest algorithm
			AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA_oid);
			//load signing algorithm
			AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);

			//Create SignerInfo:
			SignerInfo sInfo = new SignerInfo(xName, serial,
					digestAlgorithmId, signAlgorithmId, signedData);
			//Create ContentInfo:
			ContentInfo cInfo = new ContentInfo(ContentInfo.DIGESTED_DATA_OID,
					new DerValue(DerValue.tag_OctetString, signedData));
			//Create PKCS7 Signed data
			PKCS7 p7 = new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
					new X509Certificate[] { cert },
					new SignerInfo[] { sInfo });
			//Write PKCS7 to byte array
			ByteArrayOutputStream bOut = new DerOutputStream();
			p7.encodeSignedData(bOut);
			byte[] encodedPKCS7 = bOut.toByteArray();
			return encodedPKCS7;
		}
		catch (GeneralSecurityException e) {
			throw new IOException(e);
		}
	}
	
	
	public static final Properties loadProperties(String resourcePath) throws IOException {
		ClassLoader loader = StampSigner.class.getClassLoader();
		InputStream is = loader.getResourceAsStream(resourcePath);
		Properties props = new Properties();
		props.loadFromXML(is);
		is.close();
		return props;
	}
	
}
