package io.blocko.coinstack.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

import io.blocko.apache.commons.io.IOUtils;

public class StampSignerBC implements SignatureInterface {
	
	public static final String CONFIG_XML = "coinstack/coinstack-stamp.xml";
	public static final String STAMP_KEYSTORE_PATH = "stamp.keystore.path";
	public static final String STAMP_KEYSTORE_PIN = "stamp.keystore.pin";
	
	
	private PrivateKey privateKey;
	private Certificate[] certificateChain;
	
	private final void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	private final PrivateKey getPrivateKey() {
		return this.privateKey;
	}
	private final void setCertificateChain(final Certificate[] certificateChain) {
		this.certificateChain = certificateChain;
	}
	private final Certificate[] getCertificateChain() {
		return this.certificateChain;
	}
	private final Certificate getCertificate(int index) {
		return getCertificateChain()[index];
	}
	
	
	private boolean isLoaded = false;
	
	public void loadSigner() {
		loadSigner(CONFIG_XML);
	}
	public void loadSigner(String configPath) {
		try {
			Properties props = loadProperties(configPath);
			loadSigner(props);
		} catch (GeneralSecurityException e) {
			throw new IllegalArgumentException(e);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
	public void loadSigner(Properties props)
			throws GeneralSecurityException, IOException {
		String path = props.getProperty(STAMP_KEYSTORE_PATH);
		String pin = props.getProperty(STAMP_KEYSTORE_PIN);
		loadSigner(path, pin.toCharArray());
	}
	public void loadSigner(String resourcePath, char[] pin)
			throws GeneralSecurityException, IOException {
		ClassLoader cl = this.getClass().getClassLoader();
		InputStream is = cl.getResourceAsStream(resourcePath);
		loadSigner(is, pin);
		is.close();
	}
	public void loadSigner(InputStream is, char[] pin)
			throws GeneralSecurityException, IOException {
		// set flag
		isLoaded = true;
		
		// load keystore
		KeyStore keystore = KeyStore.getInstance("PKCS12");
		keystore.load(is, pin);
		
		// load private key and certificate chain
		Certificate cert = null;
		String alias;
		Enumeration<String> aliases = keystore.aliases();
		while (aliases.hasMoreElements()) {
			alias = aliases.nextElement();
			PrivateKey privkey = (PrivateKey) keystore.getKey(alias, pin);
			setPrivateKey(privkey);
			Certificate[] certChain = keystore.getCertificateChain(alias);
			if (certChain == null) {
				continue;
			}
			setCertificateChain(certChain);
			cert = certChain[0];
			if (cert instanceof X509Certificate) {
				// avoid expired certificate
				((X509Certificate) cert).checkValidity();
			}
			break;
		}
		if (cert == null) {
			throw new IOException("Could not find certificate");
		}
	}
	
	
	@Override
	public byte[] sign(InputStream content) throws IOException {
		try {
			if (!isLoaded) {
				loadSigner();
			}
            List<Certificate> certList = new ArrayList<Certificate>();
            certList.addAll(Arrays.asList(certificateChain));
            @SuppressWarnings("rawtypes")
			Store certs = new JcaCertStore(certList);
            CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
            org.bouncycastle.asn1.x509.Certificate cert = org.bouncycastle.asn1.x509.Certificate.getInstance(getCertificate(0).getEncoded());
            ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA256WithRSA").build(privateKey);
            gen.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(sha1Signer, new X509CertificateHolder(cert)));
            gen.addCertificates(certs);
            CMSProcessableInputStream msg = new CMSProcessableInputStream(content);
            CMSSignedData signedData = gen.generate(msg, false);
            return signedData.getEncoded();
			
		}
		catch (GeneralSecurityException e) {
			throw new IOException(e);
		} catch (CMSException e) {
			throw new IOException(e);
		} catch (OperatorCreationException e) {
			throw new IOException(e);
		}
	}
	
	
	public static final Properties loadProperties(String resourcePath) throws IOException {
		ClassLoader loader = StampSignerBC.class.getClassLoader();
		InputStream is = loader.getResourceAsStream(resourcePath);
		Properties props = new Properties();
		props.loadFromXML(is);
		is.close();
		return props;
	}
	
	class CMSProcessableInputStream implements CMSTypedData {
		private InputStream in;
		private final ASN1ObjectIdentifier contentType;

		CMSProcessableInputStream(InputStream is) {
			this(new ASN1ObjectIdentifier(CMSObjectIdentifiers.data.getId()), is);
		}

		CMSProcessableInputStream(ASN1ObjectIdentifier type, InputStream is) {
			contentType = type;
			in = is;
		}

		@Override
		public Object getContent() {
			return in;
		}

		@Override
		public void write(OutputStream out) throws IOException, CMSException {
			// read the content only one time
			IOUtils.copy(in, out);
			in.close();
		}

		@Override
		public ASN1ObjectIdentifier getContentType() {
			return contentType;
		}
	}
	
}
