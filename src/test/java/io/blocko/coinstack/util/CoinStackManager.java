package io.blocko.coinstack.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.util.Properties;

import io.blocko.coinstack.AbstractEndpoint;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.Endpoint;
import io.blocko.coinstack.model.CredentialsProvider;

public class CoinStackManager {
	
	public static final String CONFIG_PATH = "coinstack.xml";
	
	public static final String PROP_CLOUD_ACCESS_KEY = "coinstack.cloud.access_key";
	public static final String PROP_CLOUD_SECRET_KEY = "coinstack.cloud.secret_key";
	public static final String PROP_SERVER_ENDPOINT = "coinstack.server.endpoint";
	public static final String PROP_KEY_PRIVATEKEY = "coinstack.key.privatekey";
	public static final String PROP_KEY_ADDRESS = "coinstack.key.address";
	
	private static CoinStackManager _instance = null;
	public static CoinStackManager getInstance() {
		if (_instance == null) {
			synchronized(CoinStackManager.class) {
				if (_instance == null) {
					_instance = new CoinStackManager();
				}
			}
		}
		return _instance;
	}
	
	private Properties prop = null;
	private CoinStackClient coinstack = null;
	private String addr = null;
	private char[] key = null;
	
	private CoinStackManager() {
		this.prop = new Properties();
		try {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			InputStream in = cl.getResourceAsStream(CONFIG_PATH);
			this.prop.loadFromXML(in);
			in.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		if (prop.containsKey(PROP_CLOUD_ACCESS_KEY) && prop.containsKey(PROP_CLOUD_SECRET_KEY)) {
			String accessKey = prop.getProperty(PROP_CLOUD_ACCESS_KEY);
			String secretKey = prop.getProperty(PROP_CLOUD_ACCESS_KEY);
			this.coinstack = createCoinStackClient(accessKey, secretKey);
		}
		else if (prop.containsKey(PROP_SERVER_ENDPOINT)) {
			String endpoint = prop.getProperty(PROP_SERVER_ENDPOINT);
			this.coinstack = createCoinStackClient(endpoint);
		}
		
		if (prop.containsKey(PROP_KEY_PRIVATEKEY)) {
			setKey(prop.getProperty(PROP_KEY_PRIVATEKEY));
		}
		if (prop.containsKey(PROP_KEY_ADDRESS)) {
			setAddr(prop.getProperty(PROP_KEY_ADDRESS));
		}
	}
	
	private static CoinStackClient createCoinStackClient(final String accessKey, final String secretKey) {
		return new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getSecretKey() {
				return secretKey;
			}
			@Override
			public String getAccessKey() {
				return accessKey;
			}
		}, Endpoint.MAINNET);
	}
	private static CoinStackClient createCoinStackClient(final String endpoint) {
		return new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "0";
			}
			@Override
			public String getSecretKey() {
				return "0";
			}
		}, new AbstractEndpoint() {
			@Override
			public String endpoint() {
				return endpoint;
			}
			@Override
			public boolean mainnet() {
				return true;
			}
			@Override
			public PublicKey getPublicKey() {
				return null;
			}
		});
	}
	
	public void setKey(char[] key) {
		this.key = key;
	}
	public void setKey(String key) {
		this.key = (key == null) ? null : key.toCharArray();
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	public CoinStackClient getCoinStack() {
		return coinstack;
	}
	public String getAddr() {
		return addr;
	}
	public char[] getKey() {
		return key;
	}
}
