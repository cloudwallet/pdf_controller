package io.blocko.coinstack.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.util.StampClient;

public class TestStampRedirected {
	CoinStackClient coinstack = null;

	@Before
	public void before() {
		coinstack = CoinStackManager.getInstance().getCoinStack();
	}
	
	@Test
	public void testSkip() {
		// only for ksm public bitcoin blockchain legacy system
	}
	
	//@Test
	public void testMain() throws Exception {
		// https://blockchain.info/tx/8236888cb3d7abab50322b8c9773ea0028ea519cedd89f4d63c9b7b792cf7eef
		String stampId = "8236888cb3d7abab50322b8c9773ea0028ea519cedd89f4d63c9b7b792cf7eef-0";
		String stampData = StampClient.getStampData(coinstack, stampId);
		System.out.println("stampId="+stampId);
		System.out.println("stampData="+stampData);
		
		String redirectIndex = StampClient.getRedirectIndex(stampId);
		System.out.println("redirectIndex="+redirectIndex);
		assertEquals(redirectIndex, "1GLHoQ4CE1MSHehURVzGkzfWyAEzw5FYwc");
		
		String redirectedStampId = StampClient.getRedirectedStampId(coinstack, redirectIndex);
		System.out.println("redirectedStampId="+redirectedStampId);
		if (redirectedStampId == null || redirectedStampId.isEmpty()) {
			return;
		}
		
		String redirectedStampData = StampClient.getStampData(coinstack, stampId);
		System.out.println("redirectedStampData="+redirectedStampData);
		assertEquals(stampData, redirectedStampData);
	}
}
