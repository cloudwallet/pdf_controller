package io.blocko.coinstack.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;


import org.junit.Before;
import org.junit.Test;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Stamp;

public class TestStamp {
	
	public static String PATH_SCRATCH = "src/test/resources/scratch/";
	
	CoinStackClient coinstack = null;
	char[] pkey = null;
	
	@Before
	public void before() {
		coinstack = CoinStackManager.getInstance().getCoinStack();
		pkey = CoinStackManager.getInstance().getKey();
	}
	
	@Test
	public void main() throws Exception {
		assertFalse(pkey == null || pkey.length == 0);
		
		String original = PATH_SCRATCH + "test-original.pdf";
		String stamped = PATH_SCRATCH + "test-stamped.pdf";
		String changed = PATH_SCRATCH + "test-changed.pdf";
		long now = System.currentTimeMillis();
		stamped = PATH_SCRATCH + String.format("test-%s-stamped.pdf", SDF.format(now));
		changed = PATH_SCRATCH + String.format("test-%s-changed.pdf", SDF.format(now));
		System.out.println("original="+original);
		System.out.println("stamped="+stamped);
		System.out.println("changed="+changed);
		
		testWriteStampId(original, stamped);
		testVerifyStampId(stamped);
		testChangeStampId(stamped, changed);
		testVerifyStampId(changed);
	}
	
	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd-HHmmss");
	
	
	public String stampDocument(String hash) throws IOException, CoinStackException {
		String stampId = StampClient.stampDocument(coinstack, pkey, hash);
		return stampId;
	}
	public String getStampData(String stampId) throws IOException, CoinStackException {
		String stampHash = StampClient.getStampData(coinstack, stampId);
		return stampHash;
	}
	public int getStampConfirmations(String stampId) throws IOException, CoinStackException {
		int confirmations = StampClient.getStampConfirmations(coinstack, stampId);
		return confirmations;
	}
	public Stamp getStamp(String stampId) throws IOException, CoinStackException {
		return StampClient.getStamp(coinstack, stampId);
	}
	
	
	public void testWriteStampId(String src, String res) throws IOException, CoinStackException {
		PDFController controller = new PDFController(new File(src));
		String hash = controller.calculateHash();
		String stampId = stampDocument(hash);
		System.out.printf("[%10s] hash=%s, stampId=%s\n", "original", hash, stampId);
		controller.writeStampId(new File(res), stampId);
		controller.close();
	}
	
	public void testVerifyStampId(String src) throws IOException, CoinStackException {
		PDFController controller = new PDFController(new File(src));
		if (controller.isEncrypted()) {
			System.out.printf("[%10s] hash=%s, (document is encrypted)\n", "stamped", controller.calculateHash());
			controller.close();
			return;
		}
		if (!controller.hasStampId()) {
			System.out.printf("[%10s] hash=%s, (stamp not found)\n", "stamped", controller.calculateHash());
			controller.close();
			return;
		}
		String hash = controller.calculateHashStamped();
		String stampId = controller.getStampId();
		System.out.printf("[%10s] hash=%s, stampId=%s\n", "stamped", hash, stampId);
		controller.close();
		
		String blockchainHash = getStampData(stampId);
		int confirmations = getStampConfirmations(stampId);
		Stamp stamp = getStamp(stampId);
		assertEquals(confirmations, stamp.getConfirmations());
		System.out.printf("[%10s] hash=%s, confirmations=%d\n", "blockchain", blockchainHash, confirmations);
		assertEquals(blockchainHash, hash);
	}
	
	public void testChangeStampId(String src, String res) throws IOException, CoinStackException {
		PDFController controller = new PDFController(new File(src));
		String prevStampId = controller.getStampId();
		if (prevStampId != null) {
			String originHash = controller.calculateHashStamped();
			String newStampId = stampDocument(originHash);
			controller.changeStampId(new File(res), newStampId);
			controller.close();
		}
	}
}
